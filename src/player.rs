extern crate rand;

use self::rand::Rng;

use types;
use types::Move;
use types::CellType::*;

use ::std::io::Write;

static BOARD_SIZE:usize = 9;
static MACROBOARD_SIZE:usize = 3;

macro_rules! println_stderr(
	($($arg:tt)*) => (
		match writeln!(&mut ::std::io::stderr(), $($arg)* ) {
			Ok(_) => {},
			Err(x) => panic!("Unable to write to stderr: {}", x),
		}
	)
);

pub fn is_legal (game: &mut types::Game, row:usize, col:usize) -> bool {
	let board_index = row * BOARD_SIZE + col;
	let mbr:usize = row / MACROBOARD_SIZE;
	let mbc:usize = col / MACROBOARD_SIZE;
	let mb_index:usize = mbr * MACROBOARD_SIZE + mbc;

	return (game.board.data[board_index] == Empty) && (game.macroboard.data[mb_index] == Active);
}

pub fn update_legal_moves (game: &mut types::Game) -> () {
	for row in 0..BOARD_SIZE {
		for col in 0..BOARD_SIZE {
			let index = row * BOARD_SIZE + col;
			game.legal_moves[index] = is_legal (game, row, col);
		}
	}
}

pub fn debug_legal_moves (game: &types::Game) -> () {
	let mut result:String = "".to_string();
	let mut count:usize = 0;
	for i in 0..game.legal_moves.len() {
		if count >= BOARD_SIZE {
			result = result + "\n";
			count = 0;
		};
		count = count + 1;
		match game.legal_moves[i] {
			true => result = result + "# ",
			false => result = result + ". "
		}
	}
	/* enough time wasted trying to please the type system here, hence the code above
	for mv in game.legal_moves.iter() {
		match mv {
			true => result = result + "# ",
			false => result = result + ". "
		}
	};
	*/
	println_stderr!("{}", result);
}

pub fn num_legal_moves (game: &mut types::Game) -> usize {
	return (game.legal_moves).into_iter().fold(0, |acc, v| if *v {acc + 1} else {acc})
}

pub fn select_move(game: &mut types::Game, mut rng: rand::ThreadRng) -> types::Move {
	let num_legal:usize = num_legal_moves(game);
	let selected:usize = (rng.gen::<usize>()) % num_legal;
	let (index, _) = game.legal_moves.into_iter().fold((0, 0), 
		|(acc_all, acc_legal), v| 
		if acc_legal > selected {(acc_all, acc_legal)} 
		else if acc_legal == selected {
			if *v {
				(acc_all, acc_legal + 1)
			} else {
				(acc_all + 1, acc_legal)
			}
		}
		else {(acc_all + 1, (acc_legal + (if *v {1} else {0})))}
	);
	let row = ((index as usize) / BOARD_SIZE) as i8;
	let col = ((index as usize) % BOARD_SIZE) as i8;
	let player_move = types::Move{row : row, col : col};
	return player_move;
}

pub fn make_move(settings: &mut types::Settings, game: &mut types::Game) -> types::Move {
	let mut rng = rand::thread_rng();
	update_legal_moves(game);
	let player_move = select_move(game, rng);
	return player_move;

}

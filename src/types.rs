#[derive(Default)]
pub struct Settings {
	pub timebank: u64,
	pub time_per_move: u64,
	pub your_bot: String,
	pub your_botid: u8,
}

#[derive(Clone, Copy, PartialEq)]
pub enum CellType {
	Empty,
	Player1,
	Player2,
	Active,
	Draw,
}

#[derive(Default)]
pub struct Board {
	pub data: Box<[CellType]>,
}

#[derive(Default)]
pub struct Game {
	pub board : Board,
	pub macroboard : Board,
	pub round: u16,
	pub turn: u16,
	pub last_update: u64,
	pub last_timebank: u64,
	pub legal_moves: Box<[bool]>,
}

#[derive(Default)]
pub struct Move {
	pub row: i8,
	pub col: i8,
}

